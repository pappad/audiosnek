#!/usr/bin/env python2

import kivy
kivy.require('1.9.1')

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.slider import Slider
from kivy.core.audio import SoundLoader

import os
import cPickle as pickle
import time

def frange(start, stop, step):
    x = start
    while x < stop:
        yield x
        x += step

class SoundWidget(BoxLayout):
    # got drunk while writing this, so much lazy
    def __init__(self, parent, soundfile, **kwargs):
        super(SoundWidget, self).__init__(**kwargs)
        #print "Loading" + parent.soundfolder.text + '/' + soundfile
        self.soundfile = soundfile
        self.daddy = parent # 'parent' must be used by some pappa class

        self.orientation = 'vertical'

        self.soundbutton = ToggleButton(text=soundfile)
        self.add_widget(self.soundbutton)
        self.soundbutton.bind(state=self.toggle)

    def set_volume(self, slider, val):
        self.sound.volume = slider.value

    def toggle(self, instance, value):
        if self.soundbutton.state == 'down':
            self.sound = SoundLoader.load(self.daddy.soundfolder.text + '/' + self.soundfile)

            self.volume_slider = Slider(min=0.0, max=1.0, value=0.7, step=0.05, orientation='horizontal')
            self.add_widget(self.volume_slider)
            self.volume_slider.bind(on_touch_move=self.set_volume)

            self.sound.loop = True
            self.sound.play()
        else: # clean up to save memory
            self.remove_widget(self.volume_slider)
            self.sound.stop()
            self.sound.unload()
            del self.sound

class SoundSelect(BoxLayout):

    def __init__(self, **kwargs):
        super(SoundSelect, self).__init__(**kwargs) #kivy magic
        self.orientation = 'vertical'
        self.spacing = 5

        self.grid = GridLayout() # holds the SoundButtons
        self.grid.cols = 5 # good for large and small screen

        self.soundlist = list() # no duplicates
        self.folders = list() # quick history
        self.shortcut_button_list = list() # supposed to hold button objects

        self.add_widget(self.grid)

        self.soundfolder = TextInput(multiline=False,\
                hint_text="/home/me/sounds", \
                size=(500,150), size_hint=(0.5, None))
        self.btn_add_sounds = Button(text="Add Sounds",\
                size=(300,150), size_hint=(None,None))
        self.entry_layout = BoxLayout(spacing=5, \
                size=(800,150), size_hint=(1,None))
        self.entry_layout.add_widget(self.soundfolder) # entry area has own layout
        self.entry_layout.add_widget(self.btn_add_sounds)
        self.add_widget(self.entry_layout)

        self.shortcut_buttons = GridLayout(spacing=2, cols=3, \
                size=(800,156), size_hint=(1,None))
        self.add_widget(self.shortcut_buttons)

        self.btn_add_sounds.bind(on_release=self.add_sounds)
        try:
            self.get_folders() # this makes the shortcuts
        except IOError:
            pass

    def add_sounds(self, instance):
        try:
            test = os.listdir(self.soundfolder.text) # to see if folder exists
        except OSError:
            print "Invalid folder selection"
            return False

        if self.soundfolder.text not in self.folders: # only add as favorite if not already there
            self.record_folder(self.soundfolder.text) # add favorite
            for shortcut in self.shortcut_button_list:
                self.shortcut_buttons.remove_widget(shortcut) # delete all buttons
            self.get_folders() # make new buttons

        for sound in os.listdir(self.soundfolder.text):

            if sound.endswith("ogg") or \
                sound.endswith("wav"): pass #check for supported filetypes
            else: continue

            if sound in self.soundlist: # don't double up on sounds
                continue
            self.soundlist.append(sound)

            layout = BoxLayout()
            self.grid.add_widget(layout)

            btn_play = SoundWidget(self, soundfile=sound, text=sound)
            layout.add_widget(btn_play)

    def record_folder(self, folder):
        self.folders.append(folder)

        with open('shortcuts.pickle', 'w') as fi:
            pickle.dump(self.folders, fi)

    def get_folders(self):

        try:
            with open('shortcuts.pickle', 'r') as fi:
                self.folders = pickle.load(fi)
        except EOFError:
            return

        self.shortcut_button_list = list()
        if len(self.folders) > 3: self.folders.pop(0) # only hold 3 favorites

        for folder in self.folders:
            btn_shortcut = Button(text=folder, \
                    size=(300,150), size_hint=(0.5,None))
            self.shortcut_buttons.add_widget(btn_shortcut)
            btn_shortcut.bind(on_release=self.run_shortcut)
            self.shortcut_button_list.append(btn_shortcut)

    def run_shortcut(self, instance):
        self.soundfolder.text = instance.text # set favorite text
        self.add_sounds(None) # grab from favorites menu

class AudioSnek(App):

    def build(self): # make menu and such
        return SoundSelect()

if __name__ == '__main__':
    AudioSnek().run()
