AudioSnek
========
A python based dynamic soundboard built with the pygame and kivy frameworks.

![](https://pappad.info/media/video/AudioSnek.mp4)

Requirements:
------------
 - python2.7
 - pygame
 - kivy >= 1.10.0

How to run:
-----------
 - `python2.7 main.py`

How to use:
----------
Give it a directory, either absolute or relative to the directory being run from, press "Add sounds"
and a grid of togglebuttons will appear with file names.

The supported file types are .wav and .ogg, (I reccommend .ogg.)
Try to keep each file size under 20MB, as when multiple are loaded into pygame
they can take up a lot of memory. 20MB is sufficent for 20Min of audio at 41000Hz, ~110kbps crystal clear audio.

After pressing a togglebutton, it will illuminate blue, and your sound should play shortly. There will be a delay due to the read requried. After the sound is loaded, a slider bar will appear under the name of the file, allowing you to adjust the volume.
Pressing the illumated togglebutton will extinguish it, fading out the sound. This will also free memory.


Modification:
------------
If you want custom grid sizes, do it yourself. It's very easy. Go to the SoundSelect class, and change self.grid.cols to whatever integer of columns of sounds you want. The grid will scale with the size of the window.


